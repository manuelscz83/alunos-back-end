package com.example.caed.dao.api;

import org.springframework.data.repository.CrudRepository;

import com.example.caed.model.Aluno;

public interface AlunoDaoAPI extends CrudRepository<Aluno, Long> {

}
