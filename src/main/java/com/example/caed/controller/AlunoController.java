package com.example.caed.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.caed.model.Aluno;
import com.example.caed.api.AlunoServiceAPI;


@Controller
public class AlunoController {
	
	@Autowired
	private AlunoServiceAPI alunoServiceAPI;
	
	@RequestMapping("/")
	public String index(Model model) {
		model.addAttribute("list", alunoServiceAPI.getAll());
		return "index";
	}
	
	
	@GetMapping("/save/{ID}")
	public String showSave(@PathVariable("ID") Long id , Model model) {
		if(id != null && id != 0) {
			model.addAttribute("aluno", alunoServiceAPI.get(id));
		}else {
			model.addAttribute("aluno", new Aluno());
		}
		return "save";
	}
	
	@PostMapping("/save")
	public String save(Aluno aluno, Model model) throws Exception {
		alunoServiceAPI.save(aluno);
		return "redirect:/";
	}
	
	@GetMapping("/delete/{ID}")
	public String delete(@PathVariable Long id, Model model) {
		alunoServiceAPI.delete(id);
		
		return "redirect:/";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); 
	    // change the format according to your need.
	    dateFormat.setLenient(false);

	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	

}
