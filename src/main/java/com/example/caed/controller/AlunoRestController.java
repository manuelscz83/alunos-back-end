package com.example.caed.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.naming.directory.InvalidAttributesException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.example.caed.api.AlunoServiceAPI;
import com.example.caed.model.Aluno;


@RestController
@RequestMapping(value = "/api/v1/")
@CrossOrigin("*")
public class AlunoRestController {

	@Autowired
	private AlunoServiceAPI AlunoServiceAPI;

	@CrossOrigin("*")
	@GetMapping(value = "/all")
	public List<Aluno> getAll() {
		return AlunoServiceAPI.getAll();
	}
	
	@CrossOrigin("*")
	@GetMapping(value = "/find/{id}")
	public Aluno find(@PathVariable Long id) {
		return AlunoServiceAPI.get(id);
	}

	@CrossOrigin("*")
	@PostMapping(value = "/save")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Aluno> save(@RequestBody Aluno aluno, HttpServletResponse response) throws Exception {
		try {
			Aluno obj = AlunoServiceAPI.save(aluno);
			return new ResponseEntity<Aluno>(obj, HttpStatus.OK);
		} catch (InvalidAttributesException e) {
			response.setStatus(HttpStatus.NOT_ACCEPTABLE.value());
			throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, e.getMessage(), e);
		} catch (Exception e) {
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
		
	}
	
	@CrossOrigin("*")
	@PutMapping(value = "/save/{id}")
    @ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Aluno> save(@RequestBody Aluno aluno,@PathVariable Long id, HttpServletResponse response) throws Exception {
		try {
			Aluno old = AlunoServiceAPI.get(id);
			aluno.setDATA_CRIACAO(old.getDATA_CRIACAO());
			Aluno obj = AlunoServiceAPI.save(aluno);
			return new ResponseEntity<Aluno>(obj, HttpStatus.OK);
		} catch (InvalidAttributesException e) {
			response.setStatus(HttpStatus.NOT_ACCEPTABLE.value());
			throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, e.getMessage(), e);
		} catch (Exception e) {
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}
	
	@GetMapping(value = "/delete/{id}")
	public ResponseEntity<Aluno> delete(@PathVariable Long id) {
		Aluno aluno = AlunoServiceAPI.get(id);
		if (aluno != null) {
			AlunoServiceAPI.delete(id);
		}else {
			return new ResponseEntity<Aluno>(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<Aluno>(aluno, HttpStatus.OK);
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); 
	    // change the format according to your need.
	    dateFormat.setLenient(false);

	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}

}
