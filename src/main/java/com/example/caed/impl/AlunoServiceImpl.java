package com.example.caed.impl;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.directory.InvalidAttributesException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.caed.api.AlunoServiceAPI;
import com.example.caed.commons.GenericServiceImpl;
import com.example.caed.dao.api.AlunoDaoAPI;
import com.example.caed.enums.EnumSimNao;
import com.example.caed.model.Aluno;
import com.example.caed.utils.DataHelper;

@Service
public class AlunoServiceImpl extends GenericServiceImpl<Aluno, Long> implements AlunoServiceAPI {

	@Autowired
	private AlunoDaoAPI alunoDaoAPI;
	
	@Override
	public AlunoDaoAPI getDao() {
		return alunoDaoAPI;
	}

	@Override
	public Aluno save(Aluno entity) throws Exception{
		
		
		if (!validate(entity.getEMAIL())){
			System.out.println("Email inválido");
	        throw new InvalidAttributesException("Email inválido");
		}
		else {
		
			Date hoje = new Date();
			if (entity.getID() == null)
				entity.setDATA_CRIACAO(hoje);
			else
				entity.setDATA_ALTERACAO(hoje);
			
			System.out.println(entity.getDATA_NASCIMENTO().toString());
			int diferenca = DataHelper.getDiffYears(entity.getDATA_NASCIMENTO(), hoje);
			System.out.println("Diferenca: " + Integer.toString(diferenca)); 
			if (diferenca >= 18)
				entity.setMAIOR_IDADE(EnumSimNao.SIM);
			else
				entity.setMAIOR_IDADE(EnumSimNao.NAO);
			
			System.out.println("Maior de idade: " + entity.getMAIOR_IDADE().toString()); 
	
			return super.save(entity);
		}
	}
	
	
	
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
		    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

	public static boolean validate(String emailStr) {
		if(emailStr != "") {
	        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
	        return matcher.find();
		}else return true;
	}
	


}
