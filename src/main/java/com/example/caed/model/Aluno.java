package com.example.caed.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;

import com.example.caed.enums.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
public class Aluno {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ID;
	
	@Column(length = 100)
    @Size(max = 100)
	private String NOME;
	
	@Column(length = 11)
    @Size(max = 11)
	private String CPF;
	
	@Column
	@JsonFormat(pattern="dd/MM/yyyy")
	private Date DATA_NASCIMENTO;
	
	@Column
	private EnumSexo SEXO;
	
	@Column
	@JsonProperty(required = false)
	private String EMAIL;
	
	@Column
	@JsonProperty(required = false)
	private EnumSimNao MAIOR_IDADE;
	
	@Column
	@JsonProperty(required = false)
	private Date DATA_CRIACAO;
	
	@Column
	@JsonProperty(required = false)
	private Date DATA_ALTERACAO;

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public String getNOME() {
		return NOME;
	}

	public void setNOME(String nOME) {
		NOME = nOME;
	}

	public String getCPF() {
		return CPF;
	}

	public void setCPF(String cPF) {
		CPF = cPF;
	}

	public Date getDATA_NASCIMENTO() {
		return DATA_NASCIMENTO;
	}

	public void setDATA_NASCIMENTO(Date dATA_NASCIMENTO) {
		DATA_NASCIMENTO = dATA_NASCIMENTO;
	}

	public EnumSexo getSEXO() {
		return SEXO;
	}

	public void setSEXO(EnumSexo sEXO) {
		SEXO = sEXO;
	}

	public String getEMAIL() {
		return EMAIL;
	}

	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}

	public Date getDATA_ALTERACAO() {
		return DATA_ALTERACAO;
	}

	public void setDATA_ALTERACAO(Date dATA_ALTERACAO) {
		DATA_ALTERACAO = dATA_ALTERACAO;
	}

	public EnumSimNao getMAIOR_IDADE() {
		return MAIOR_IDADE;
	}

	public void setMAIOR_IDADE(EnumSimNao mAIOR_IDADE) {
		MAIOR_IDADE = mAIOR_IDADE;
	}

	public Date getDATA_CRIACAO() {
		return DATA_CRIACAO;
	}

	public void setDATA_CRIACAO(Date dATA_CRIACAO) {
		DATA_CRIACAO = dATA_CRIACAO;
	}

	
	
}
	
