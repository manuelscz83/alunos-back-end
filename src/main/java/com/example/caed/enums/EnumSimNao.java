package com.example.caed.enums;

public enum EnumSimNao {
	SIM(1),
	NAO(0);
	
	public int getCode() {
		return code;
	}

	private EnumSimNao(int code) {
		this.code = code;
	}

	private final int code;
}
