package com.example.caed.api;

import com.example.caed.commons.GenericServiceAPI;
import com.example.caed.model.Aluno;

public interface AlunoServiceAPI extends GenericServiceAPI<Aluno, Long>  {
	
}
